# AR Coder

## Description

Using QR-codes to reach augmented reality content. Which means after reading the QR-codes with your phone, it will open a url to some AR.js content. Then your phone will use the camera to find out the position of the marker and display 3d content on top of it.

## How to run the app

1. clone the app to your local

2. yarn install

3. yarn start

## Code Documentation

Dashboard page as the core of the application. First there is a validation check if the user is logged in then the page redirected to create page, if not then the authentication page is rendered. Inside the authentication page there is only two functions used, handleChange (to change the state of data when input is changed), onSubmitLogin (to submit the login form data to the backend) and onSubmitLoginGoogle (to submit data fetch from the google api). After the submit action the state of isLoggedIn changed to true then the create page will be rendered inside dashboard page. 

The dashboard page now consist of header, sidebar, and footer. Header consist of the displays a welcome user pleasantaries and a bell for notification along with the user fullname. Sidebar consist of Create Feature, Manage Feature, and Settings. Footer shows a copyright of the website.

Create Feature have three components inside the content from the three steps when creating the AR-Codes. First, upload file component which inside you can upload file by clicking the dragger or drag and drop the file on top of the dragger. When the file is inside form data, there will be an extension check whether the format is appropriate or not. If it is then the button that was once disabled is now functional used to upload the file and continue to the second step. If not, the form notified that your file isn't valid and you need to upload the right format file. Second, the setting component have three inputs and a preview. The four inputs are for the project names, scale, height, and position. Every input has a handleChange (called when onChange triggered to change the state of the settingData) along with built-in validation check from the ant.design. When all of settingData is not empty, the validation check will pass so when the button is clicked (onSubmitSetting will be called), it will send the settingData to the backend and fetch the AR-Code then continue to the next step. Third, it will display the finished AR-Code and there is a button bellow as an action to download the AR-Code.

Manage Feature data will be called when the manage component is rendered which is when the Manage in Sidebar is clicked. Manage have four elements. One, a title which is All AR-Codes. Two, a table which displayed all AR-Codes. Three and four are sort and filter buttons. Each row of data have an action in Download column when you click it will download the AR-Code in the row data using the handleActionArTableDownload with the key as parameter.

## Splits of CSS Codes
1. Restyle => restyling from an existing styles, i.e. ant-design or HTML default.
2. ClassStyling => styling for a new element (without any style embedded to it) and styling with new added classes so we can call it anywhere we'd like to use.
3. Spacing => styling for spacing, i.e. margins and paddings. Also contains responsive spacings.
4. Responsive => Responsive displays of elements.