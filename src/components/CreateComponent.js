import React, { Component } from 'react';
import { 
	Row, 
	Col,
	Steps,
} from 'antd';

import CreateUploadFileComponent from './CreateUploadFileComponent';
import CreateSettingComponent from './CreateSettingComponent';
import CreateCompleteComponent from './CreateCompleteComponent';

import uploadIcon from '../assets/img/icon/upload-active.png';
import settingIcon from '../assets/img/icon/setting.png';
import settingActiveIcon from '../assets/img/icon/setting-active.png';
import downloadIcon from '../assets/img/icon/download.png';
import downloadActiveIcon from '../assets/img/icon/download-active.png';

const { Step } = Steps;

export class CreateComponent extends Component {

	render() {
		return (
			<div>
				<div className="mr-10 ml-10 background-white shadow-bottom-right-light-blue mb-4">
					<Steps className="margin-auto width-70" progressDot current={this.props.step}>
						<Step>
						</Step>
						<Step/>
						<Step/>
					</Steps>
					<Row className="align-items-center text-center pb-1">
						<Col span={8}>
							<div className="font-weight-bold text-14 text-tosca" level={4}>
								<img className="step-icon mr-2" src={uploadIcon} />
								Upload
							</div>
						</Col>
						<Col span={8} className="text-center">
							<div className={this.props.step > 0 ? "font-weight-bold text-14 text-tosca" : "font-weight-bold text-14 text-light-blue"}>
								<img className="step-icon mr-2" src={this.props.step > 0 ? settingActiveIcon : settingIcon} />
								Setting
							</div>
						</Col>
						<Col span={8} className="text-16 font-weight-normal">
							<div className={this.props.step > 1 ? "font-weight-bold text-14 text-tosca" : "font-weight-bold text-14 text-light-blue"} level={4}>
								<img className="step-icon mr-2" src={this.props.step > 1 ? downloadActiveIcon : downloadIcon} />
								Complete
							</div>
						</Col>
					</Row>
				</div>
				<div className="border-light-blue border-rad-6 mr-10 ml-10 py-4 px-4">
					{
						{
							0 : 
								<CreateUploadFileComponent
									alert={this.props.alert}
									fileList={this.props.fileList} 
									uploadFileError={this.props.uploadFileError} 
									handleBeforeUpload={this.props.handleBeforeUpload} 
									onSubmitFile={this.props.onSubmitFile} 
								/>,
							1 : 
								<CreateSettingComponent
									alert={this.props.alert}
									createSettingData={this.props.createSettingData} 
									handleChange={this.props.handleChange}
									prevStep={this.props.prevStep} 
									onSubmitCreateSetting={this.props.onSubmitCreateSetting} 
								/>,
							2 : 
								<CreateCompleteComponent
									alert={this.props.alert}
									prevStep={this.props.prevStep}
									completedFile={this.props.completedFile} 
									handleActionDownload={this.props.handleActionDownload} 
								/>
						}[this.props.step]
					}
				</div>
		</div>
		)
	}
}

export default CreateComponent;
