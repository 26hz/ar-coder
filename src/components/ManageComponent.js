import React, { Component } from 'react';
import { 
	Row, 
	Col,
	Table,
	Input,
	Alert,
} from 'antd';
import {
	FilterFilled,
	DownloadOutlined,
} from '@ant-design/icons';

export class ManageComponent extends Component {

	render() {
		const columns = [
			{
				title: 'Name',
				dataIndex: 'fullname',
				sorter: (a, b) => (a.fullname.toLowerCase() > b.fullname.toLowerCase()) ? 1 : (a.fullname.toLowerCase() === b.fullname.toLowerCase()) ? (a.id.toLowerCase() > b.id.toLowerCase() ? 1 : -1) : -1,
				sortDirections: ['descend', 'ascend'],
			},
			{
				title: 'ID',
				dataIndex: 'id',
				sorter: (a, b) => (a.id.toLowerCase() > b.id.toLowerCase()) ? 1 : (a.id.toLowerCase() === b.id.toLowerCase()) ? (a.date.toLowerCase() > b.date.toLowerCase() ? 1 : -1) : -1,
				sortDirections: ['descend', 'ascend'],
			},
			{
				title: '# Scan',
				dataIndex: 'scan',
				sorter: (a, b) => a.scan - b.scan,
				sortDirections: ['descend', 'ascend'],
			},
			{
				title: 'Date',
				dataIndex: 'date',
				sorter: (a, b) => (a.date.toLowerCase() > b.date.toLowerCase()) ? 1 : (a.date.toLowerCase() === b.date.toLowerCase()) ? (a.date.toLowerCase() > b.date.toLowerCase() ? 1 : -1) : -1,
				sortDirections: ['descend', 'ascend'],
				render: date => (
					<div className="d-inline-grid">
						<div className="d-none">
							{
								date = date.split(",")
							}
						</div>
						<span className="font-weight-bold">{date[0]}</span>
						<span className="font-weight-light text-gray">{date[1]}</span>
					</div>
				)
			},
			{
				title: 'State',
				dataIndex: 'state',
				sorter: (a, b) => (a.state.toLowerCase() > b.state.toLowerCase()) ? 1 : (a.state.toLowerCase() === b.state.toLowerCase()) ? (a.date.toLowerCase() > b.date.toLowerCase() ? 1 : -1) : -1,
				sortDirections: ['descend', 'ascend'],
				render: (state) => (
					<div className={state === "LIVE" ? "tag-blue" : "tag-red"}>
						{state}
					</div>),
			},
			{
				title: 'Download',
				dataIndex: '',
				key: 'download',
				render: (text, record) => (
					<span onClick={ () => this.props.handleActionArTableDownload(record.key)}>
						<DownloadOutlined className="download-icon-table" />
					</span>
				),
			},
		];

		return (
			<div className="px-3">
				<Row className="px-3">
					<Col flex="120px" className="font-weight-bold text-sm-center text-16 flex-xs-100 pr-3" >All AR-code</Col>
					<Col flex="auto" className="flex-xs-50">
						<Input className="filter-input" onChange={this.props.handleFilter} prefix={<FilterFilled />} />
					</Col>
				</Row>

				{this.props.alert.message &&
					<Alert className="my-3" message={this.props.alert.message} type={this.props.alert.type} showIcon/>
				}

				<Table 
					dataSource={this.props.displayedArData}
					columns={columns}
				>
				</Table>
      </div>
		)
	}
}

export default ManageComponent;
