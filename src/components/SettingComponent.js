import React, { Component } from 'react'
import {
	Button, 
	Form,
	Input,
	Radio,
	Modal,
	Alert,
} from 'antd';

export class SettingComponent extends Component {
	render() {
		return (
			<div className="px-4">

				{this.props.alert.message &&
					<Alert className="mb-3" message={this.props.alert.message} type={this.props.alert.type} showIcon/>
				}
				<Form
					id="settingForm"
					className="px-4"
					onFinish={this.props.onSubmitSetting}
					onFieldsChange={ (changedFields) => this.props.handleChange(changedFields, "settingData") }
					initialValues={{
						email: this.props.settingData.email,
						username: this.props.settingData.username,
						newsletter: this.props.settingData.newsletter
					}}
					hideRequiredMark
					scrollToFirstError
				>
					<Form.Item
						label="User Name"
						className="mt-3"
						name="username"
						rules={[
							{
								required: true,
								message: 'Please input your username!',
							},
						]}
					>
						<Input
							value={this.props.settingData.username}
						/>
					</Form.Item>

					<Form.Item
						label="Email Address"
						name="email"
						rules={[
							{
								type: 'email',
								message: 'The input is not a valid email!',
							},
							{
								required: true,
								message: 'Please input your email!',
							},
						]}
					>
						<Input 
							value={this.props.settingData.email} 
							className="width-100"
						/>
					</Form.Item>

					<Form.Item
						label="Password"
						name="password"
					>
						<div onClick={this.props.showModal} className="tag-orange">Change</div>
					</Form.Item>

					<Form.Item
						label="Email Subscription"
						name="newsletter"
						rules={[
							{
								required: true,
								message: 'Please set whether you want to subscibe to our newsletter or not!',
							},
						]}
					>
						<Radio.Group className="font-weight-normal" value={this.props.newsletter}>
							<Radio value={true}>Subscribe</Radio>
							<Radio value={false}>Unsubscribe</Radio>
						</Radio.Group>
					</Form.Item>

					<Button form="settingForm" key="submit" htmlType="submit" type="primary">Update</Button>
				</Form>
				<Modal
					wrapClassName="settingModal"
					closable={false}
					visible={this.props.passwordModalVisible}
					onCancel={this.props.hideModal}
          footer={[
            <Button className="tag-orange" form="settingPasswordForm" key="submit" htmlType="submit" type="primary" >
              Change Password
            </Button>,
          ]}
        >
					{this.props.alert.message &&
						<Alert className="mb-3" message={this.props.alert.message} type={this.props.alert.type} showIcon/>
					}
          <Form
						id="settingPasswordForm"
						className="px-3"
						onFinish={(e) => this.props.onSubmitSetting(e, 'password')}
						onFieldsChange={ (changedFields) => this.props.handleChange(changedFields, "settingPasswordData") }
						hideRequiredMark
						scrollToFirstError
					>
						<Form.Item
							label="Old Password"
							className="mt-3"
							name="oldPassword"
							rules={[
								{
									required: true,
									message: 'Please input your old password!',
								},
							]}
						>
							<Input.Password
								value={this.props.settingPasswordData.oldPassword}
							/>
						</Form.Item>

						<Form.Item
							label="New Password"
							name="newPassword"
							rules={[
								{
									required: true,
									message: 'Please input your new password!',
								},
							]}
						>
							<Input.Password
								value={this.props.settingPasswordData.newPassword} 
								className="width-100"
							/>
						</Form.Item>

						<Form.Item
							label="Retype Password"
							name="confirmation"
							dependencies={['newPassword']}
							rules={[
								{
									required: true,
									message: 'Please confirm your password!',
								},
								({ getFieldValue }) => ({
									validator(rule, value) {
										if (!value || getFieldValue('newPassword') === value) {
											return Promise.resolve();
										}
										return Promise.reject('The two passwords that you entered do not match!');
									},
								}),
							]}
						>
							<Input.Password
								value={this.props.settingPasswordData.confirmation} 
								className="width-100"
							/>
						</Form.Item>
					</Form>
        </Modal>
      </div>
		)
	}
}

export default SettingComponent;