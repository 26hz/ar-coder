import React, { Component } from 'react';
import GoogleLogin from 'react-google-login';

import { 
	Row, 
	Col, 
	Typography,
	Layout, 
	Button, 
	Divider, 
	Input, 
	Form,
	Alert,
} from 'antd';
import { GoogleOutlined } from '@ant-design/icons';

import logo from '../assets/img/logo.png';
import widget from '../assets/img/auth-widget.png';

const { Title } = Typography;

class AuthenticationPage extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Layout className="auth-page">
				<Row>
					<Col className="h-100 min-h-100vh max-h-100vh background-white absolute-center-parent" xs={{span : 24, order : 2}} sm={{span : 24, order : 2}} md={{span : 8, order : 1}}>
						<div className="text-center absolute-center-child width-80">
							<img className="auth-logo" src={logo} />
							<Title className="font-weight-bold my-3" level={2}>Welcome to AR Coder!</Title>

						
							{this.props.alert.message &&
								<Alert className="mt-2" message={this.props.alert.message} type={this.props.alert.type} showIcon/>
							}

							<Form
								className="auth-page-form"
								onFieldsChange={ (changedFields) => this.props.handleChange(changedFields, "userData") }
								onFinish={this.props.onSubmitLogin}
								hideRequiredMark
								scrollToFirstError
							>
								<Form.Item
									className="mt-3 mb-3"
									name="email"
									rules={[
										{
											type: 'email',
											message: 'The input is not a valid email!',
										},
										{
											required: true,
											message: 'Please input your email!',
										},
									]}
								>
									<Input placeholder="example@gmail.com" />
								</Form.Item>

								<Form.Item
									className="mb-4"
									name="password"
									rules={[
										{
											required: true,
											message: 'Please input your password!',
										},
									]}
								>
									<Input.Password />
								</Form.Item>

								<Form.Item className="border-none">
									<Button className="font-weight-bold mb-3 text-white background-tosca" block htmlType="submit">
										LOG IN
									</Button>

									<Button className="font-weight-bold text-white background-light-blue" block htmlType="submit">
										SIGN UP
									</Button>
								</Form.Item>
							</Form>
						</div>
						<div className="auth-terms text-gray">By signing up to ARcoder, you agree to our terms of service.</div>
					</Col>
					<Col span={16} className="auth-background" xs={{span : 24, order : 1}} sm={{span : 24, order : 1}} md={{span : 16, order : 2}}>
						<Title className="font-weight-bold text-white text-right auth-text-widget-position" level={2}>Making AR Accessible to EVERYONE</Title>
						<img className="auth-img-widget-position" src={widget} />
						<div className="text-gray text-12 font-weight-bold auth-credit">
							Powered by <a href="https://www.26hz.io/">26Hz.io</a>
						</div>
					</Col>
				</Row>
			</Layout>
		)
	}
}

export default AuthenticationPage;