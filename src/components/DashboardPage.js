import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { history } from '../helper/history';
import { userActions } from '../actions/user.actions';
import { displayActions } from '../actions/display.actions';
import { createActions } from '../actions/create.actions';
import { manageActions } from '../actions/manage.actions';

import { 
	Row, 
	Col, 
	Typography,
	Layout,
	Menu,
} from 'antd';
import {
	SettingFilled,
	BellFilled,
} from '@ant-design/icons';

import AuthenticationPage from './AuthenticationPage';
import CreateComponent from './CreateComponent';
import ManageComponent from './ManageComponent';
import SettingComponent from './SettingComponent';

import logo from '../assets/img/logo.png';
import lightbulbIcon from '../assets/img/icon/lightbulb.png';
import bookIcon from '../assets/img/icon/book.png';

const { Header, Content, Footer, Sider } = Layout;
const { Title } = Typography;

class DashboardPage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			userData : {
				email: '',
				password: ''
			},
			createSettingData: {
				projectname : '',
				scale: '',
				height: '',
				position: '',
			},
			settingData:{
				username: '',
				email: '',
				newsletter: true,
			},
			settingPasswordData: {
				oldPassword: '',
				newPassword: '',
				confirmation: '',
			},
			passwordModalVisible: false,
		};
	}

	handleChange = (event, objectName="") => {
		if(objectName !== "" && typeof event === "object"){
			let objectVal = this.state[objectName];
			Object.keys(event).forEach((eventItem) => {
				objectVal[event[eventItem].name] = event[eventItem].value;
			});
			this.setState({
				[objectName] : objectVal
			});
		} else {
			this.setState({
				[event.target.name]: event.target.value,
			});
		}
	}

	changeMenu = (page) => {
		this.props.setActive(page, this.props.arData);
		if(this.state.settingData.username === ""){
			const settingData = this.props.settingData;
			this.setState({
				settingData
			});
		}
	}
	
	handleFilter = (e) => {
		const searchValue = e.target.value.trim();
		let displayedArData = this.props.arData;
    displayedArData = displayedArData.filter(o => Object.keys(o).some(k => String(o[k]).toLowerCase().includes(searchValue.toLowerCase())));
		this.props.filter(displayedArData);
  };
	
	handleBeforeUpload = fileList => {
		const name = fileList.name;
		const validationFormat = /^.*\.(jpg|jpeg|JPG|gtlf|glb|png|gif)$/;
		const checkFileFormat = validationFormat.test(name)
		const progressElement = document.querySelector('.ant-upload-drag');
		const progressBarUploadFile = document.querySelector('.ant-upload-list.ant-upload-list-text');
		progressElement.append(progressBarUploadFile);
		if(checkFileFormat){
			this.props.addFileSuccess(fileList);
			return true;
		} else {
			this.props.addFileFailed();
			return false;
		}
	}

	onSubmitLogin = () => {
		this.props.login(this.state.userData);
	};

	onSubmitLoginGoogle = (response) => {
		if(!("key" in response)){
			this.props.login(this.state.userData);
		}
	}

	onActiveDrag = (e) => {
		e.preventDefault();
		this.props.drag();
	}

	onInactiveDrag = (e) => {
		e.preventDefault();
		this.props.notDrag();
	}

	onSubmitFile = (e) => {
		const progressBarUploadFile = document.querySelector('.ant-upload-list.ant-upload-list-text');
		progressBarUploadFile.className += " infront-element";

		this.props.step1Submit();
	}

	showModal = () => {
		this.setState({
      passwordModalVisible: true,
    });
	};

	hideModal = () => {
		let settingPasswordData = this.state.settingPasswordData;
		Object.keys(settingPasswordData).forEach(v => settingPasswordData[v] = '');
		this.setState({
			passwordModalVisible: false,
			settingPasswordData
    });
	};

	onSubmitSetting = (setting, type = '') => {
		if(type === 'password'){
			this.props.submitPassword(setting);
		} else {
			this.props.submitSetting(setting);
		}
	};

	handleActionDownload = () => {
		//handle action download in create step 3
  }

  handleActionArTableDownload = (key) => {
		//handle action download in manage table
  }

	render() {
		return (
			<div>
        { !this.props.isLoggedIn ? (
					<AuthenticationPage
						alert={this.props.alert}
						userData={this.state.userData} 
						onSubmitLogin={this.onSubmitLogin}
						onSubmitLoginGoogle={this.onSubmitLoginGoogle}
						handleChange={this.handleChange}
						handleToggleLogin={this.handleToggleLogin}
					/>
					) : (
						<Layout>
							<Sider
								className="background-white min-h-100vh"
								breakpoint="md"
								collapsedWidth="0"
							>
								<img className="sider-logo my-3" src={logo} />
								<Menu className="text-gray" mode="inline">
									<Menu.Item 
										onClick={() => this.changeMenu('create', this.props.arData)}
										className={this.props.activePage === "create" ? "active py-1 my-0" : "py-1 my-0"}
									>
										<img className="icon mr-3" src={lightbulbIcon} />
										<span className="text-14 nav-text">Create</span>
									</Menu.Item>
									<Menu.Item 
										onClick={() => this.changeMenu('manage')}
										className={this.props.activePage === "manage" ? "active py-1 my-0" : "py-1 my-0"}
									>
										<img className="icon mr-3" src={bookIcon} />
										<span className="text-14 nav-text">Manage</span>
									</Menu.Item>
									<Menu.Item 
										onClick={() => this.changeMenu('setting')}
										className={this.props.activePage === "setting" ? "active position-absolute absolute-bottom py-1 my-0" : "position-absolute absolute-bottom py-1 my-0"}
									>
										<SettingFilled />
										<span className="text-14 nav-text">Setting</span>
									</Menu.Item>
								</Menu>
							</Sider>
							<Layout>
								<Header className="background-almost-white px-4 py-4 h-auto line-height-initial">
									<Row justify="center" className="align-items-center display-desktop">
										<Col sm={16}>
											<Title className="font-weight-bold text-light-gray" level={4}>Welcome to our BETA</Title>
										</Col>
										<Col sm={2} className="border-right-light-blue text-right pr-4">
											<BellFilled />
										</Col>
										<Col sm={6} className="text-16 font-weight-normal pl-4">
											Lucas
										</Col>
									</Row>
									<div className="width-100 text-align-center display-mobile">
										<Title className="font-weight-bold text-light-gray" level={4}>Welcome to our BETA</Title>
									</div>
									<div className="margin-auto width-fit display-mobile">
										<div className="border-right-light-blue pr-4">
											<BellFilled />
										</div>
										<div className="text-16 font-weight-normal pl-4">
											Lucas
										</div>
									</div>
								</Header>
								<Content className="site-layout-background background-almost-white">
                  {
                    {
                      "create" : 
												<CreateComponent
													alert={this.props.alert}
													step={this.props.step}
													fileList={this.props.fileList}
													uploadFileError={this.props.uploadFileError}
													completedFile={this.props.completedFile} 
													isActiveDrag={this.props.isActiveDrag}
													prevStep={this.props.prevStep}
													createSettingData={this.state.createSettingData} 
													onActiveDrag={this.onActiveDrag} 
													onInactiveDrag={this.onInactiveDrag}
													handleBeforeUpload={this.handleBeforeUpload}
													onSubmitFile={this.onSubmitFile}
													handleChange={this.handleChange}
													onSubmitCreateSetting={() => this.props.step2Submit(this.state.createSettingData)}
													handleActionDownload={this.handleActionDownload}
												/>,
                      "manage" : 
												<ManageComponent
													alert={this.props.alert}
													arData={this.props.arData}
													displayedArData={this.props.displayedArData}
													handleChange={this.handleChange}
													handleFilter={this.handleFilter}
													handleActionArTableDownload={this.handleActionArTableDownload} 
												/>,
											"setting": 
												<SettingComponent
													alert={this.props.alert}
													settingData={this.state.settingData}
													settingPasswordData={this.state.settingPasswordData}
													passwordModalVisible={this.state.passwordModalVisible}
													handleChange={this.handleChange}
													showModal={this.showModal}
													hideModal={this.hideModal}
													onSubmitSetting={this.onSubmitSetting}
												/>
                    }[this.props.activePage]
                  }
								</Content>
								<Footer className="text-gray text-right text-12 font-weight-bold">Powered by <a href="https://www.26hz.io/">26Hz.io</a></Footer>
							</Layout>
						</Layout>
					)}
      </div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		//general
		alert: state.alert,
		activePage: state.display.activePage,
		//auth
		isLoggedIn: state.user.isLoggedIn,
		settingData: state.user.data,
		//create
		step: state.create.step,
		fileList: state.create.fileList,
		isActiveDrag: state.create.isActiveDrag,
		uploadFileError: state.create.uploadFileError,
		errorMessage: state.create.errorMessage,
		completedFile: state.create.completedFile,
		//manage
		arData: state.manage.arData,
		displayedArData: state.manage.displayedArData,
	}
}

const mapDispatchToProps = (dispatch) => {
  return {
		//general
		setActive: (page, manageData) => {
			dispatch(displayActions.setActive(page, manageData))
		},
		//auth
		login: (creds) => dispatch(userActions.login(creds)),
		//create
		nextStep: () => dispatch(createActions.nextStep()),
		prevStep: () => dispatch(createActions.prevStep()),
		drag: () => dispatch(createActions.drag()),
		notDrag: () => dispatch(createActions.notDrag()),
		addFileFailed: () => dispatch(createActions.addFileFailed()),
		addFileSuccess: (fileList) => dispatch(createActions.addFileSuccess(fileList)),
		step1Submit: (fileList) => dispatch(createActions.step1Submit(fileList)),
		step1SubmitFailed: () => dispatch(createActions.step1SubmitFailed()),
		step2Submit: (setting) => dispatch(createActions.step2Submit(setting)),
		step2SubmitFailed: () => dispatch(createActions.step2SubmitFailed()),
		//manage
		filter: (newList) => dispatch(manageActions.filter(newList)),
		//setting
		submitSetting: (data) => dispatch(userActions.submitSetting(data)),
		submitPassword: (data) => dispatch(userActions.submitPassword(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DashboardPage);
