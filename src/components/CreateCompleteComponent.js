import React, { Component } from 'react'
import { 
	Row, 
	Col,
	Button,
	Alert,
} from 'antd';

import ARCode from '../assets/img/arcode.png'

export class CreateSettingComponent extends Component {
	
	render() {
		return (
			<div>
				<div className=""><img src={ARCode}/></div>

				{this.props.alert.message &&
					<Alert className="mb-3" message={this.props.alert.message} type={this.props.alert.type} showIcon/>
				}
				
				<Row className="mt-3 align-items-center">
					<Col className="instruction-text text-gray" flex="auto">
						Download the AR-code, print it or paste to your website to see magic happen
					</Col>
					<Col className="mt-xs-3 flex-xs-100 d-flex" flex="200px">
						<Button className="prev-button mr-3" onClick={this.props.prevStep}>Previous</Button>
						<Button onClick={this.props.handleActionDownload} type="primary">Download</Button>
					</Col>
				</Row>
      </div>
		)
	}
}

export default CreateSettingComponent;