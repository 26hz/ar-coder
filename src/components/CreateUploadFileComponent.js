import React, { Component } from 'react'
import { 
	Row, 
	Col,
	Button,
	Upload,
	Alert,
} from 'antd';
import {
	FolderFilled,
	FolderOpenTwoTone,
} from '@ant-design/icons';

const { Dragger } = Upload;

export class CreateUploadFileComponent extends Component {

	render() {
		return (
			<div>
				{
					this.props.uploadFileError ? 
						Object.entries(this.props.alert).length !== 0 ?
							<Alert className="mb-3" message={this.props.alert.message} type={this.props.alert.type} showIcon/>
							: <Alert className="mb-3" message="File Format isn't supported" type="error" showIcon/> : ''
				}
        <Dragger
					className={this.props.uploadFileError ? 'error' : ''}
					name="file"
					beforeUpload={this.props.handleBeforeUpload}
				>
					<Row className="align-items-center">
						<Col span={8}>
							<p className="ant-upload-drag-icon mb-0">
								{this.props.isActiveDrag ?
									<FolderOpenTwoTone /> :
									<FolderFilled />
								}
							</p>
						</Col>
						<Col span={16}>
							{
								this.props.fileList === null ? 
									<p className="ant-upload-text"> Drop your files here or <span className="text-blue">Browse</span> </p> : 
									<p className="ant-upload-text"> {this.props.fileList.name} </p>
							}
						</Col>
					</Row>
				</Dragger>
				
				<Row className="mt-3 align-items-center">
					<Col className="instruction-text text-gray" flex="auto">
						Support File Formats: .gtlf/.jpg/.jpeg/.JPG/.gtlf/.glb/.png/.gif
					</Col>
					<Col className="mt-xs-3 flex-xs-100" flex="85px">
						<Button disabled={this.props.fileList === null} onClick={this.props.onSubmitFile} type="primary">Upload</Button>
					</Col>
				</Row>
      </div>
		)
	}
}

export default CreateUploadFileComponent;
