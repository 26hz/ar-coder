import React, { Component } from 'react'
import { 
	Row, 
	Col,
	Button, 
	Form,
	Input,
	InputNumber,
	Select,
	Alert,
} from 'antd';
const { Option } = Select;

export class CreateSettingComponent extends Component {
	render() {
		return (
			<div>
				<Row className="mt-3 align-items-center">
					{this.props.alert.message &&
						<Alert className="mb-3" message={this.props.alert.message} type={this.props.alert.type} showIcon/>
					}
					<Col xs={{span : 24, order : 2}} sm={{span : 12, order : 1}} className="pr-5 pr-xs-0">
						<Form
							id="createSettingForm"
							onFinish={this.props.onSubmitCreateSetting}
							onFieldsChange={ (changedFields) => this.props.handleChange(changedFields, "createSettingData") }
							hideRequiredMark
							scrollToFirstError
						>
							<Form.Item
								label="Project Name"
								className="mt-3 mb-2"
								name="projectname"
								rules={[
									{
										required: true,
										message: 'Please input your project name!',
									},
								]}
							>
								<Input
									value={this.props.createSettingData.projectname}
								/>
							</Form.Item>

							<Form.Item
								label="Scale"
								name="scale"
								className="mb-2"
								rules={[
									{
										required: true,
										message: 'Please input scale!',
									},
								]}
							>
								<InputNumber 
									value={this.props.createSettingData.scale} 
									className="width-100" 
									min={1}
								/>
							</Form.Item>

							<Form.Item
								label="Height"
								name="height"
								className="mb-2"
								rules={[
									{
										required: true,
										message: 'Please input height!',
									},
								]}
							>
								<InputNumber 
									value={this.props.createSettingData.height} 
									className="width-100" 
									min={1}
								/>
							</Form.Item>

							<Form.Item
								name="position"
								label="Position"
								rules={[
									{
										required: true,
										message: 'Please input position!',
									},
								]}
							>
								<Select
									value={this.props.createSettingData.position}
									allowClear
								>
									<Option value="0">0 degrees</Option>
									<Option value="45">45 degrees</Option>
									<Option value="90">90 degrees</Option>
								</Select>
							</Form.Item>
						</Form>
					</Col>
					<Col xs={{span : 24, order : 1}} sm={{span : 12, order : 2}}>
						
					</Col>
				</Row>

				<Row className="mt-3 align-items-center">
					<Col className="instruction-text text-gray" flex="auto">
						instruction text section
					</Col>
					<Col className="mt-xs-3 flex-xs-100 d-flex" flex="170px">
						<Button className="prev-button mr-3" onClick={this.props.prevStep}>Previous</Button>
						<Button form="createSettingForm" key="submit" htmlType="submit" type="primary">Create</Button>
					</Col>
				</Row>
      </div>
		)
	}
}

export default CreateSettingComponent;