const initialState = {
	step: 0,
	fileList: null,
	isActiveDrag: false,
	uploadFileError: false,
	createSettingData: {
		projectname : '',
		scale: '',
		height: '',
		position: '',
	},
	completedFile: null,
};

export default function authentication(state = initialState, action) {
	switch (action.type) {
		case "NEXT_STEP":
			return {
				...state,
				step: state.step+1,
			};
		case "PREV_STEP":
			return {
				...state,
				step: state.step-1,
			};
		case "DRAGGED":
			return {
				...state,
				isActiveDrag: true,
			};
		case "NOT_DRAGGED":
			return {
				...state,
				isActiveDrag: false,
			};
		case "ADD_FILE_FAILED":
			return {
				...state,
				uploadFileError: true,
			};
		case "ADD_FILE_SUCCESS":
			return {
				...state,
				fileList: action.fileList,
				uploadFileError: false,
			};
		case "STEP1_SUBMIT":
			return {
				...state,
				uploadFileError: false,
			};
		case "STEP1_SUBMIT_FAILED":
			return {
				...state,
				uploadFileError: true,
			};
		case "STEP2_SUBMIT":
			return {
				...state,
				createSettingData: action.createSettingData,
			};
		case "STEP2_SUBMIT_FAILED":
			return {
				...state,
				createSettingData: {},
			};
		default:
			return state
	}
}