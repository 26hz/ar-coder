const initialState = {
	activePage: ''
};

export default function authentication(state = initialState, action) {
	switch (action.type) {
		case "SET_ACTIVE":
			return {
				activePage: action.page,
			};
		default:
			return state
	}
}