const initialState = {
	arData: [],
	displayedArData: [],
};
  
  export default function authentication(state = initialState, action) {
    switch (action.type) {
      case "SET_DATA":
        return {
					arData: action.data,
					displayedArData: action.data,
        };
      case "FILTER":
        return {
					...state,
					displayedArData: action.newList,
        };
      default:
        return state
    }
  }