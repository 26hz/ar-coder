const initialState = {
  isLoggedIn: false,
  data: {}
};

export default function authentication(state = initialState, action) {
  switch (action.type) {
    case "LOGIN_REQUEST":
      return {
        isLoggedIn: false,
        data: action.data,
      };
    case "LOGIN_SUCCESS":
      return {
        isLoggedIn: true,
        data: action.data,
      };
    case "LOGIN_FAILURE":
      return {
        isLoggedIn: false,
        data: {}
      };
    case "LOGOUT":
      return {
        isLoggedIn: false,
        data: {}
      };
    default:
      return state
  }
}