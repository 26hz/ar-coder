
import { combineReducers } from "redux";
import alert from './alert.reducer';
import display from './display.reducer';
import user from './user.reducer';
import create from './create.reducer';
import manage from './manage.reducer';

const rootReducers = combineReducers({
	alert,
	display,
	user,
	create,
	manage,
})

export default rootReducers;