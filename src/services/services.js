import axios from 'axios';
import { API_URL } from '../config';

export const service = {
  login: function(creds) {
    return axios.post(API_URL + '/auth', { email: creds.email, password: creds.password });
  },
  createSubmit1: function (data) {
    return axios.post(API_URL + '/file-submit', { data: data });
  },
  createSubmit2: function (form) {
    return axios.post(API_URL + '/file-setting', { form: form });
  },
  getCompletedFile: function () {
    return axios.get(API_URL + '/file-download');
  },
  getManageData: function(){
    return axios.get(API_URL + '/manage');
  },
  submitSetting: function(data){
    return axios.post(API_URL + '/setting', { data: data });
  },
  submitPassword: function(data){
    return axios.post(API_URL + '/setting-password', { data: data });
  }
}
