import { alertActions } from './alert.actions';
import { service } from '../services/services';

const nextStep = () => {
	return { type: "NEXT_STEP" };
}

const prevStep = () => {
	return { type: "PREV_STEP" };
}

const drag = () => {
	return { type: "DRAGGED" };
}

const notDrag = () => {
	return { type: "NOT_DRAGGED" };
}

const addFileFailed = () => {
	return { type: "ADD_FILE_FAILED" };
}

const addFileSuccess = (fileList) => {
	return { type: "ADD_FILE_SUCCESS", fileList };
}

const step1Submit = (fileList) => {
	return dispatch => {
		dispatch(request({ fileList }));
		dispatch(alertActions.clear());
		
		service.createSubmit1(fileList)
			.then(
				() => { 
					dispatch(nextStep());
					dispatch(alertActions.clear());
				},
				error => {
					dispatch(failure(error.toString()));
					dispatch(alertActions.error(error.toString()));
				}
			);
	};

	function request(fileList) { return { type: "STEP1_SUBMIT", fileList } }
	function failure(error) { return { type: "STEP1_SUBMIT_FAILED", error } }
}

const step2Submit = (data) => {
	return dispatch => {
		dispatch(request({ data }));
		dispatch(alertActions.clear());
		
		service.createSubmit2(data)
			.then(
				() => { 
					dispatch(nextStep());
					dispatch(alertActions.clear());
				},
				error => {
					dispatch(failure(error.toString()));
					dispatch(alertActions.error(error.toString()));
				}
			);
	};

	function request(fileList) { return { type: "STEP2_SUBMIT", fileList } }
	function failure(error) { return { type: "STEP2_SUBMIT_FAILED", error } }
}

export const createActions = {
	nextStep,
	prevStep,
	drag, 
	notDrag, 
	addFileFailed, 
	addFileSuccess, 
	step1Submit, 
	step2Submit,
};
  