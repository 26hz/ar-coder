import { alertActions } from './alert.actions';
import { manageActions } from './manage.actions';
import { service } from '../services/services';

const setActive = (page, manageData = []) => {
	return dispatch => {
		dispatch(set(page));
		dispatch(alertActions.clear());

		if(manageData.length === 0){
			service.getManageData()
				.then(
					data => {
						dispatch(manageActions.setData(data.data));
					},
					error => {
						dispatch(alertActions.error(error.toString()));
					}
				);
		}
	};

	function set(page) { return { type: "SET_ACTIVE", page } }
}

export const displayActions = {
	setActive,
};
  