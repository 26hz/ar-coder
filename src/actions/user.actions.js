import { alertActions } from './alert.actions';
import { displayActions } from './display.actions';
import { service } from '../services/services';

const login = (creds) => {
	return dispatch => {
		dispatch(request({ creds }));
		
		service.login(creds)
			.then(
					data => {
						dispatch(displayActions.setActive("create"));
						dispatch(success(data.data));
					},
					error => {
						dispatch(failure(error.toString()));
						dispatch(alertActions.error(error.toString()));
					}
			);
  };

	function request(creds) { return { type: "LOGIN_REQUEST", creds } }
	function success(data) { return { type: "LOGIN_SUCCESS", data } }
	function failure(error) { return { type: "LOGIN_FAILURE", error } }
}

const logout = () => {
	return { type: "LOGOUT" };
}

const submitSetting = (data) => {
	return dispatch => {
		dispatch(request({ data }));
		dispatch(alertActions.clear());

		service.submitSetting(data)
			.then(
					() => {
						dispatch(alertActions.success("You've successfully changed your info."));
					},
					error => {
						dispatch(alertActions.error(error.toString()));
					}
			);
  };

	function request(data) { return { type: "SUBMIT_SETTING", data } }
}

const submitPassword = (data) => {
	return dispatch => {
		dispatch(request({ data }));
		dispatch(alertActions.clear());
		
		service.submitPassword(data)
			.then(
					() => {
						dispatch(alertActions.success("You've successfully changed your password."));
					},
					error => {
						dispatch(alertActions.error(error.toString()));
					}
			);
  };

	function request(data) { return { type: "SUBMIT_PASSWORD", data } }
}

export const userActions = {
	login,
	logout,
	submitSetting,
	submitPassword,
};