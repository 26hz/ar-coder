const setData = (data) => {
	return { type: "SET_DATA", data };
}

const filter = (newList) => {
	return { type: "FILTER", newList };
}

export const manageActions = {
	setData,
	filter,
};
