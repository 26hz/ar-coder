const submitSetting = (data) => {
	return { type: "SUBMIT_SETTING", data };
}

const submitPassword = (data) => {
	return { type: "SUBMIT_PASSWORD", data };
}

export const settingActions = {
	submitSetting,
	submitPassword,
};
