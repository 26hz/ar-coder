import React from 'react';
import { Route, Switch } from 'react-router-dom';
import DashboardPage from './components/DashboardPage';
import 'antd/dist/antd.css';
import './assets/css/spacing.css';
import './assets/css/restyle.css';
import './assets/css/classStyling.css';
import './assets/css/responsive.css';

const App = () => {
  return (
    <Switch>
      <Route path="/" component={DashboardPage} />
    </Switch>
  )
}

export default App;
